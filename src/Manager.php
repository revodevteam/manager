<?php
namespace Framework\Manager;

use Exception;
use Framework\Manager\Onion;
use Framework\Http\Routing\Route;
use Framework\DInjector\DInjector;
use Framework\Http\Routing\Router;
use Framework\Http\Request\Request;
use Framework\Template\TemplateInterface;

final class Manager
{

    /**
     * @var \Framework\Http\Routing\Router 
     */
    protected $router;

    /**
     * @var \Framework\DInjector\DInjector 
     */
    protected $injector;
    protected $extensions;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->injector = new DInjector();
        $this->loadExtensions();
    }

    protected function loadExtensions()
    {
        if (file_exists(APP_DIR . DS . 'extensions.php')) {
            $this->extensions = require APP_DIR . DS . 'extensions.php';
        } else {
            $this->extensions = [];
        }
    }

    public function manage()
    {
        $responseObj = $this->runThroughMiddleWare();

        if ($responseObj instanceof TemplateInterface) {
            $responseObj->render();
        }
    }

    protected function runThroughMiddleWare()
    {
        /** @var Route $task */
        $task = $this->router->getMatchedRoute();

        $middleWares = $this->prepareMiddlewareStack($task);

        if (!empty($middleWares)) {

            $onion = new Onion($this->injector, $middleWares);

            return $onion->peel($this->injector->resolve(Request::class), function() use($task) {

                    return $this->callController($task);
                });
        }

        return $this->callController($task);
    }

    protected function prepareMiddlewareStack(Route $task)
    {
        $middlewareGroups = $this->extensions['middlewares'] ?? [];

        if (!empty($task->middlewares)) {
            $middleWares = [];
            $groupAssigned = false;

            foreach ($task->middlewares as $middleware) {
                if (isset($middlewareGroups[$middleware])) {
                    $middleWares = array_merge($middleWares, $middlewareGroups[$middleware]);
                    $groupAssigned = true;
                    continue;
                }

                $middleWares[] = $middleware;
            }

            $middleWares = array_filter(($groupAssigned === true) ? $middleWares : array_merge(($middlewareGroups['web'] ?? []), $middleWares));
        } else {

            /**
             * If no middleware found,
             * assign web group as default
             */
            $middleWares = $middlewareGroups['web'] ?? [];
        }

        return $middleWares;
    }

    /**
     * Time to load controller class
     * We will try to resolve via our injector
     * 
     * @param Route $route
     * @return type
     */
    protected function callController(Route $route)
    {
        return $this->injector->resolve($route->controller, $route->method);
    }
}
